import discord
from discord.ext import tasks
import yaml
import time, datetime
import asyncio
import random
import sys, os, subprocess
from traceback import format_exc

client = discord.Client()

with open('config.yml', 'r') as file:
    config = yaml.load(file.read(), Loader=yaml.CLoader)

with open('users.yml', 'r') as file:
    users = yaml.load(file.read(), Loader=yaml.CLoader)

CONNECTED = None


def timestamp(timestamp):
    data = [[86400, 'jours'], [3600, 'heures'], [60, 'minutes'], [1, 'secondes']]
    mod = 100000000
    for k in range(len(data)):
        mod, data[k][0] = data[k][0], ((timestamp % mod) // data[k][0])
        if data[k][0] == 1:
            data[k][1] = data[k][1][:-1]
    return ' et '.join([f'{k[0]} {k[1]}' for k in data if k[0]][:2])


def timestamp_from_date(date1, date2=None):
    if not date2:
        date2 = time.time()
    timestamp_ = (int(max(date1, date2) - min(date1, date2)))
    return timestamp(timestamp_)


def save_users():
    with open('users.yml', "w") as file:
        file.write(yaml.dump(users, Dumper=yaml.CDumper))


async def log(msg, level='info', user=None):
    colors = {'win': discord.Color.purple(), 'status': discord.Color.lighter_grey(),
              'transcript': discord.Color.blurple(), 'console': discord.Color.dark_grey(),
              'info': discord.Color.orange(), 'low_info': discord.Color.gold(), 'progress': discord.Color.green(),
              'error': discord.Color.red(), 'ask_help': discord.Color.teal()}
    if not user:
        user = client.user
    print(f'{time.strftime("%Y-%d-%m %H:%M:%S")} [{level}] - {user} - {msg}')

    embed = discord.Embed(title=level, description=msg, color=colors[level])
    embed.set_author(name=f'{user} - {user.id}', icon_url=user.avatar_url)
    embed.timestamp = datetime.datetime.now(datetime.timezone.utc)
    if level in ("console", "status"):
        channel = config['specials_channels']['console']
    elif level == "transcript":
        channel = config['specials_channels']['transcript']
    else:
        channel = config['specials_channels']['log']
    await client.get_channel(channel).send(embed=embed)
    if level in ('error', 'ask_help', 'win'):
        await client.get_channel(channel).send(f'<@{config["game_master"]}>')


async def reply(channel, content, **kwargs):
    if isinstance(content, list):
        content = random.choice(content)
    if isinstance(channel, discord.Message):
        channel = channel.channel
    if content:
        await channel.trigger_typing()
        await asyncio.sleep(config['bot_write_delay'])
        await channel.send(content.format(**kwargs))


async def connect_user(connect=True, user=None, notify=True):
    global CONNECTED
    if connect:
        if CONNECTED:
            await connect_user(False, notify=True)
        if not user.dm_channel:
            await user.create_dm()
        if notify:
            await reply(user.dm_channel, config['msg']['transcript']['connected'])
        CONNECTED = user.dm_channel
    else:
        if notify:
            await reply(CONNECTED, config['msg']['transcript']['disconnected'])
        CONNECTED = False


@client.event
async def on_ready():
    await log(f'Logged in !', level='status')
    global STATUS
    STATUS = None
    if not change_status.is_running():
        change_status.start()


@tasks.loop(seconds=60)
async def change_status():
    global STATUS
    item = ((int(time.time()) - config['status']['start_time']) // config['status']['timespan']) % len(
        config['status']['msg'])
    status = config['status']['msg'][item]
    if item != STATUS:
        await log(f"Change status to {status} (#{item})", "low_info")
    await client.change_presence(activity=discord.Game(status))
    STATUS = item


@client.event
async def on_error(evt, *args, **kwargs):
    error = format_exc()
    if args and isinstance(args[0], discord.Message):
        await log(f'An error occured: {error}', user=args[0].author, level='error')
        await reply(args[0], config['msg']['error'])
    else:
        await log(f'An error occured: {error}, {args}, {kwargs}', level='error')


@client.event
async def on_message(message):
    global config
    global users
    if message.channel == CONNECTED:
        await log(message.content, level="transcript", user=message.author)
    if message.author == client.user:
        return

    if message.channel.id == config['specials_channels']['transcript'] and CONNECTED:
        await message.delete()
        await CONNECTED.send('> ' + message.content)

    if message.channel.id == config['specials_channels']['console']:

        if message.content == "shutdown":
            save_users()
            if CONNECTED:
                await connect_user(False)
            await client.change_presence(status=discord.Status.invisible)
            await log("Shutting down...", "status")
            await client.logout()

        elif message.content == "restart":
            save_users()
            if CONNECTED:
                await connect_user(False)
            await client.change_presence(status=discord.Status.invisible)
            await log("Restarting...", "status")
            await client.logout()
            os.execv(sys.executable, ['python'] + sys.argv)

        elif message.content == "reload":
            with open('config.yml', 'r') as file:
                config = yaml.load(file.read(), Loader=yaml.CLoader)
            await log("Reloading...", "status")

        elif message.content == "update":
            if CONNECTED:
                await connect_user(False)
            process = subprocess.Popen(["git", "pull"])
            while process.poll() is None:
                await asyncio.sleep(1)
            process.terminate()
            await log("Updated ! Please restart if necessary...", "status")

        elif message.content.startswith("connect"):
            args = message.content.split(" ")
            try:
                user = client.get_user(int(args[1]))
            except:
                await log("No user found", "console")
                return
            await connect_user(user=user)
            await log(f"Started transcript with {user}", "console")
            return

        elif message.content == "disconnect":
            if not CONNECTED:
                await log("Not connected", "console")
                return
            await log(f"Stopping transcript with {CONNECTED.recipient}", "console")
            await connect_user(False)
            return

        elif message.content.startswith("reset"):
            args = message.content.split(" ")[1:]
            if args == ['all']:
                users = {}
                save_users()
                await log(f'All usersdata deleted !', 'console')
                return
            if args == ['winner']:
                if users.get('winner'):
                    del users['winner']
                    save_users()
                    await log(f'Winner deleted !', 'console')
                    return
                else:
                    await log(f'No winner found', 'console')
                    return
            try:
                del users[int(args[0])]
                save_users()
                await log(f'User deleted successfully !', 'console')
            except:
                await log(f"No corresponding userdata found", "console")
                return

        elif message.content.startswith('send'):
            args = message.content.split(" ")
            msg = message.content[5:]
            try:
                user = client.get_user(int(args[1]))
            except:
                await log("No user found", "console")
                return
            if not user.dm_channel:
                await user.create_dm()
            await reply(user.dm_channel, f'> {" ".join(args[2:])}')
            await log(f'Sent message: {" ".join(args[2:])}', level='console', user=user)

        elif message.content == 'list_users':
            dict_users = users
            list_users = []
            for k in dict_users.keys():
                if k != 'winner':
                    dict_users[k].update({"id": k})
                    list_users.append(dict_users[k])
            list_users.sort(key=lambda x: x['challenge'], reverse=True)
            await log('\n'.join(
                [f'{client.get_user(k["id"])}: level {k["challenge"]} ({k["available_hints"]} available_hints)'
                 for k in list_users]),
                'console'
            )

    if message.author.dm_channel == message.channel:
        user = message.author.id
        if time.time() > config['start_time']:
            authorized = True
        elif user in config['admins']:
            authorized = True
        else:
            authorized = False
        if authorized:
            if not message.channel == CONNECTED:
                if not users.get(user):  # Commandes demandant à ce que les défis n'aient pas commencés
                    bonjour = False
                    for k in config['bonjour_words']:
                        if k in message.content.lower().split(" "):
                            bonjour = True
                    if bonjour:
                        users[user] = {'challenge': 0, 'available_hints': config['hints']['max_amount']}
                        await log(f'Started the game', user=message.author, level='progress')
                        save_users()
                        await reply(message, config['msg']['start'], hints=config['hints']['max_amount'])
                        await reply(message, config['msg']['help_command'])
                        await reply(message, config['challenges'][0]['intro'], data=config['challenges'][0]['data'])
                        return None

                    await reply(message, config['msg']['not_found_unstarted'])
                    return None

            current_challenge = users[user]['challenge']

            if message.content.startswith('code'):
                code = message.content[5:].lower()
                correct_code = config['challenges'][current_challenge]['code']
                if not isinstance(correct_code, list):
                    correct_code = [correct_code]
                if code not in correct_code:
                    await log(
                        f'Sent a wrong answer ({code}) for challenge {current_challenge} ({config["challenges"][current_challenge]["code"]})',
                        user=message.author,
                        level='low_info'
                    )
                    await reply(message, config['msg']['bad_code'])
                    return None
                await reply(message, config['challenges'][current_challenge]['reply'])
                await log(f'Passed challenge {current_challenge}', user=message.author, level='progress')
                current_challenge += 1
                if len(config['challenges']) == current_challenge:
                    if not users.get('winner') or users.get('winner') == user:
                        await log(f'First to win the game !', user=message.author, level='win')
                        users['winner'] = user
                        save_users()
                        await reply(message, config['msg']['finished_first'])
                        return None
                    await log(f'Won the game (but not first) !', user=message.author, level='win')
                    await reply(message, config['msg']['finished_not_first'])
                    return None
                if users[user].get('hint'):
                    del users[user]['hint']
                users[user]['challenge'] = current_challenge
                save_users()
                await reply(message, config['challenges'][current_challenge]['intro'],
                            data=config['challenges'][current_challenge]['data'])
                return None

            elif message.content.startswith('ask_help'):
                await log(f"Asked for help: {message.content}", "ask_help", message.author)
                await reply(message, config['msg']['ask_help'])
                return

            elif message.content == "hint":
                if not config['challenges'][current_challenge].get('hint'):
                    await reply(message, config['msg']['hint']['no_hint'])
                    return None
                hint = users[user].get('hint')
                if not hint:
                    if users[user]['available_hints']:
                        users[user]["hint"] = int(
                            time.time() + random.randint(config['hints']['delay']['min'], config['hints']['delay']['max']))
                        users[user]['available_hints'] -= 1
                        save_users()
                        await log(
                            f'Asked for an hint (challenge {current_challenge}), waiting until {time.ctime(users[user]["hint"])}',
                            user=message.author,
                            level='low_info'
                        )
                        await reply(message, config['msg']['hint']['info'], delay=timestamp_from_date(users[user]["hint"]))
                    else:
                        await reply(message, config['msg']['hint']['not_available'], hints=config['hints']['max_amount'])
                    return None
                if time.time() < hint:
                    await reply(message, config['msg']['hint']['too_early'], delay=timestamp_from_date(users[user]['hint']))
                    return None
                await log(f'Sending to {message.author} ({user}) the hint for challenge {current_challenge}',
                          user=message.author, level='info')
                await reply(message, config['challenges'][current_challenge]['hint'])
                return None

            elif message.content == "info":
                hint = users[user].get('hint')
                await reply(message, config['msg']['infos']['level'], level=current_challenge)
                await reply(message, config['msg']['infos']['intro'], intro=config['challenges'][current_challenge]['data'])
                if not hint:
                    await reply(message, config['msg']['infos']['no_hint'])
                elif hint > time.time():
                    await reply(message, config['msg']['infos']['hint_waiting'],
                                delay=timestamp_from_date(users[user]['hint']))
                else:
                    await reply(message, config['msg']['infos']['hint'],
                                hint=config['challenges'][current_challenge]['hint'])
                await reply(message, config['msg']['infos']['remaining_hints'], amount=users[user]['available_hints'])
                return None

            elif message.content == "help":
                await reply(message, config['msg']['help_command'])
                return None

            else:
                if not message.channel == CONNECTED:
                    await reply(message, config['msg']['not_found_started'])
                    await reply(message, config['msg']['help_command'])
                    return None


client.run('NzQzNzMxMTc4Njk1MTYzOTU1.XzY7og.GwqXlP0hvbrAmwb289tF2bjcwKk')
